package collo.luca.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class Usuario {
    //1. Atributos privados

    private Administrador administrador;
    private ArrayList<Atleta> atletas;
    //2. Método constructor

    public Usuario() {
    }

    public Usuario(String nombre, String primerApellido, String segundoApellido, String identificacion, String pais, String nombreUsuario, String clave) {
        this.administrador = new Administrador(nombre, primerApellido, segundoApellido, identificacion, pais, nombreUsuario, clave);
        this.atletas = new ArrayList<>();
    }

    //3. Métodos get y set

    public Administrador getAdministrador() {
        return administrador;
    }

    public void setAdministrador(Administrador administrador) {
        this.administrador = administrador;
    }

    //Esto se hace así? Hice un forEach en toString para el array de atletas.
    public void agregarAtleta(String nombre, String primerApellido, String segundoApellido, String identificacion, String nombreUsuario, String clave, LocalDate fechaNacimiento, int edad, String genero, Direccion direccion, ArrayList<MetodoPago> metodoPago, ArrayList<Reto> reto) {
        Atleta atleta = new Atleta (nombre, primerApellido, segundoApellido, identificacion,nombreUsuario, clave, fechaNacimiento, edad, genero, direccion, metodoPago, reto);
        atletas.add(atleta);
    }

    //4. Método toString

    public String toString() {
        String listaUsuarios = "Administrador: " + administrador + "\n";
        listaUsuarios += "Usuarios atletas: \n";

        for(Atleta atleta: atletas){
            listaUsuarios+= atleta.toString() + "\n";
        }
        return listaUsuarios;
    }
}
