package collo.luca.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class Reto {

    //1. Atributos privados
    private String id;
    private String nombre;
    private String descripcion;
    private String foto;
    private String puntoInicio;
    private String puntoFinal;
    private double kilTotales;
    private double kilRecorridos;
    private String medalla;
    private double tiempoMedalla;
    private double costoReto;
    private int semanasFinReto;
    private TipoActividad tipoActividad;
    private ArrayList<Hito> hitos;

    //2. Método constructor

    public Reto() {
    }

    public Reto(String id, String nombre, String descripcion, String foto, String puntoInicio, String puntoFinal, double kilTotales, double kilRecorridos, String medalla, double tiempoMedalla, double costoReto, int semanasFinReto) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.foto = foto;
        this.puntoInicio = puntoInicio;
        this.puntoFinal = puntoFinal;
        this.kilTotales = kilTotales;
        this.kilRecorridos = kilRecorridos;
        this.medalla = medalla;
        this.tiempoMedalla = tiempoMedalla;
        this.costoReto = costoReto;
        this.semanasFinReto = semanasFinReto;
        this.hitos = new ArrayList<>();
    }

    public Reto(String id, String nombre, String descripcion, String foto, String puntoInicio, String puntoFinal, double kilTotales, double kilRecorridos, String medalla, double tiempoMedalla, double costoReto, int semanasFinReto, TipoActividad tipoActividad) {
        this.id = id;
        this.nombre = nombre;
        this.descripcion = descripcion;
        this.foto = foto;
        this.puntoInicio = puntoInicio;
        this.puntoFinal = puntoFinal;
        this.kilTotales = kilTotales;
        this.kilRecorridos = kilRecorridos;
        this.medalla = medalla;
        this.tiempoMedalla = tiempoMedalla;
        this.costoReto = costoReto;
        this.semanasFinReto = semanasFinReto;
        this.tipoActividad = tipoActividad;
        this.hitos = new ArrayList<>();
    }

    //3. Métodos get y set

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getFoto() {
        return foto;
    }

    public void setFoto(String foto) {
        this.foto = foto;
    }

    public String getPuntoInicio() {
        return puntoInicio;
    }

    public void setPuntoInicio(String puntoInicio) {
        this.puntoInicio = puntoInicio;
    }

    public String getPuntoFinal() {
        return puntoFinal;
    }

    public void setPuntoFinal(String puntoFinal) {
        this.puntoFinal = puntoFinal;
    }

    public double getKilTotales() {
        return kilTotales;
    }

    public void setKilTotales(double kilTotales) {
        this.kilTotales = kilTotales;
    }

    public double getKilRecorridos() {
        return kilRecorridos;
    }

    public void setKilRecorridos(double kilRecorridos) {
        this.kilRecorridos = kilRecorridos;
    }

    public String getMedalla() {
        return medalla;
    }

    public void setMedalla(String medalla) {
        this.medalla = medalla;
    }

    public double getTiempoMedalla() {
        return tiempoMedalla;
    }

    public void setTiempoMedalla(double tiempoMedalla) {
        this.tiempoMedalla = tiempoMedalla;
    }

    public double getCostoReto() {
        return costoReto;
    }

    public void setCostoReto(double costoReto) {
        this.costoReto = costoReto;
    }

    public int getSemanasFinReto() {
        return semanasFinReto;
    }

    public void setSemanasFinReto(int semanasFinReto) {
        this.semanasFinReto = semanasFinReto;
    }

    public TipoActividad getTipoActividad() {
        return tipoActividad;
    }

    public void setTipoActividad(TipoActividad tipoActividad) {
        this.tipoActividad = tipoActividad;
    }


    //Esto se hace así? Hice un forEach en toString del array de hitos
    //Metodo para agregar hitos
    public void agregarHito(String nombre, Double kilometro, String imagen, String descripcion, String referenciaLugar) {
        Hito hito = new Hito (nombre, kilometro, imagen, descripcion, referenciaLugar);
        hitos.add(hito);
    }


    //4. Método toString

    public String toString() {
        String listaHitos= "";
        for(Hito hito: hitos){
            listaHitos += hito.toString() + "\n";
        }

        return "Reto{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", foto='" + foto + '\'' +
                ", kilTotales=" + kilTotales +
                ", medalla='" + medalla + '\'' +
                ", tiempoMedalla=" + tiempoMedalla +
                ", costoReto=" + costoReto +
                ", semanasFinReto=" + semanasFinReto +
                ", tipoActividad=" + tipoActividad +
                ", hito=" + listaHitos +
                '}';

}

  }
