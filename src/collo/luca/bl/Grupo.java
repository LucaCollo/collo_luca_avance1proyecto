package collo.luca.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class Grupo {

    //1. Atributos privados
    private String nombre;
    private String imagen;
    private ArrayList<Atleta> participantes;
    private Reto reto;

    //2. Método constructor
    public Grupo() {
    }

    public Grupo(String nombre, String imagen) {
        this.nombre = nombre;
        this.imagen = imagen;
    }

    public Grupo(String nombre, String imagen, ArrayList<Atleta> participantes, Reto reto) {
        this.nombre = nombre;
        this.imagen = imagen;
        this.participantes = participantes;
        this.reto = reto;
    }
    //3. Métodos get y set
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public ArrayList<Atleta> getParticipantes() {
        return participantes;
    }

    public void setParticipantes(ArrayList<Atleta> participantes) {
        this.participantes = participantes;
    }

    public Reto getReto() {
        return reto;
    }

    public void setReto(Reto reto) {
        this.reto = reto;
    }

    //4. Método toString

    public String toString() {
        return "Grupo{" +
                "nombre='" + nombre + '\'' +
                ", imagen='" + imagen + '\'' +
                ", participantes=" + participantes +
                ", reto=" + reto +
                '}';
    }
}
