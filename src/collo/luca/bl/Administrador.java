package collo.luca.bl;

public class Administrador {

    //1. Atributos privados
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String identificacion;
    private String pais;
    private String nombreUsuario;
    private String clave;

    //2. Método constructor

    public Administrador() {
    }

    public Administrador(String nombre, String primerApellido, String segundoApellido, String identificacion, String pais, String nombreUsuario, String clave) {
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.identificacion = identificacion;
        this.pais = pais;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
    }

    //3. Métodos get y set

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getPais() {
        return pais;
    }

    public void setPais(String pais) {
        this.pais = pais;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    //4. Método toString

    public String toString() {
        return "Administrador{" +
                "nombre='" + nombre + '\'' +
                ", primerApellido='" + primerApellido + '\'' +
                ", segundoApellido='" + segundoApellido + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", pais='" + pais + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", clave='" + clave + '\'' +
                '}';
    }
}
