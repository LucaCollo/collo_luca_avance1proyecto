package collo.luca.bl;

public class TipoActividad {

    //1. Atributos privados

    private String id;
    private String nombre;
    private String icono;

    //2. Método constructor

    public TipoActividad() {
    }

    public TipoActividad(String id, String nombre, String icono) {
        this.id = id;
        this.nombre = nombre;
        this.icono = icono;
    }

    //3. Métodos get y set

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getIcono() {
        return icono;
    }

    public void setIcono(String icono) {
        this.icono = icono;
    }


    //4. Método toString

    public String toString() {
        return "TipoActividad{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                ", icono='" + icono + '\'' +
                '}';
    }
}
