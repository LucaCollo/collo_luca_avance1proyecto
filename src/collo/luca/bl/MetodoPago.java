package collo.luca.bl;

import java.time.LocalDate;

public class MetodoPago {

    //1. Atributos privados
    private String numTarjeta;
    private String proveedorTarjeta;
    private LocalDate fechaVenc;
    private int codigoSeguridad;

    //2. Método constructor

    public MetodoPago() {
    }

    public MetodoPago(String numTarjeta, String proveedorTarjeta, LocalDate fechaVenc, int codigoSeguridad) {
        this.numTarjeta = numTarjeta;
        this.proveedorTarjeta = proveedorTarjeta;
        this.fechaVenc = fechaVenc;
        this.codigoSeguridad = codigoSeguridad;
    }

    //3. Métodos get y set

    public String getNumTarjeta() {
        return numTarjeta;
    }

    public void setNumTarjeta(String numTarjeta) {
        this.numTarjeta = numTarjeta;
    }

    public String getProveedorTarjeta() {
        return proveedorTarjeta;
    }

    public void setProveedorTarjeta(String proveedorTarjeta) {
        this.proveedorTarjeta = proveedorTarjeta;
    }

    public LocalDate getFechaVenc() {
        return fechaVenc;
    }

    public void setFechaVenc(LocalDate fechaVenc) {
        this.fechaVenc = fechaVenc;
    }

    public int getCodigoSeguridad() {
        return codigoSeguridad;
    }

    public void setCodigoSeguridad(int codigoSeguridad) {
        this.codigoSeguridad = codigoSeguridad;
    }

    //4. Método toString

    public String toString() {
        return "MetodoPago{" +
                "numTarjeta='" + numTarjeta + '\'' +
                ", proveedorTarjeta='" + proveedorTarjeta + '\'' +
                ", fechaVenc=" + fechaVenc +
                ", codigoSeguridad=" + codigoSeguridad +
                '}';
    }
}
