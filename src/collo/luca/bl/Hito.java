package collo.luca.bl;

public class Hito {

    //1. Atributos privados

    private String nombre;
    private Double kilometro;
    private String imagen;
    private String descripcion;
    private String referenciaLugar;

    //2. Método constructor

    public Hito() {
    }

    public Hito(String nombre, Double kilometro, String imagen, String descripcion, String referenciaLugar) {
        this.nombre = nombre;
        this.kilometro = kilometro;
        this.imagen = imagen;
        this.descripcion = descripcion;
        this.referenciaLugar = referenciaLugar;
    }


    //3. Métodos get y set

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public Double getKilometro() {
        return kilometro;
    }

    public void setKilometro(Double kilometro) {
        this.kilometro = kilometro;
    }

    public String getImagen() {
        return imagen;
    }

    public void setImagen(String imagen) {
        this.imagen = imagen;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getReferenciaLugar() {
        return referenciaLugar;
    }

    public void setReferenciaLugar(String referenciaLugar) {
        this.referenciaLugar = referenciaLugar;
    }


    //4. Método toString

    public String toString() {
        return "Hito{" +
                "nombre='" + nombre + '\'' +
                ", kilometro=" + kilometro +
                ", imagen='" + imagen + '\'' +
                ", descripcion='" + descripcion + '\'' +
                ", referenciaLugar='" + referenciaLugar + '\'' +
                '}';
    }
}
