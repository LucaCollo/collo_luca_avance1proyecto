package collo.luca.bl;

import java.time.LocalDate;
import java.util.ArrayList;

public class Atleta {

    //1. Atributos privados
    private String nombre;
    private String primerApellido;
    private String segundoApellido;
    private String identificacion;
    private String nombreUsuario;
    private String clave;
    private LocalDate fechaNacimiento;
    private int edad;
    private String genero;
    private Direccion direccion;
    private ArrayList<MetodoPago> metodosPago;
    private ArrayList<Reto> retos;

    //2. Método constructor
    public Atleta() {
    }

    public Atleta(String nombre, String primerApellido, String segundoApellido, String identificacion, String nombreUsuario, String clave, LocalDate fechaNacimiento, int edad, String genero, Pais pais, Provincia provincia, Canton canton, Distrito distrito) {
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.identificacion = identificacion;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.genero = genero;
        this.direccion = new Direccion(pais, provincia, canton, distrito);
        this.metodosPago= new ArrayList<>();
    }

    public Atleta(String nombre, String primerApellido, String segundoApellido, String identificacion, String nombreUsuario, String clave, LocalDate fechaNacimiento, int edad, String genero, Pais pais, Provincia provincia, Canton canton, Distrito distrito, ArrayList<Reto> reto) {
        this.nombre = nombre;
        this.primerApellido = primerApellido;
        this.segundoApellido = segundoApellido;
        this.identificacion = identificacion;
        this.nombreUsuario = nombreUsuario;
        this.clave = clave;
        this.fechaNacimiento = fechaNacimiento;
        this.edad = edad;
        this.genero = genero;
        this.direccion = new Direccion(pais, provincia, canton, distrito);
        this.metodosPago= new ArrayList<>();
        this.retos = retos;
    }

    public Atleta(String nombre, String primerApellido, String segundoApellido, String identificacion, String nombreUsuario, String clave, LocalDate fechaNacimiento, int edad, String genero, Direccion direccion, ArrayList<MetodoPago> metodoPago, ArrayList<Reto> reto) {
    }

    public Atleta(String numTarjeta, String proveedorTarjeta, LocalDate fechaVenc, int codigoSeguridad) {
    }

    //3. Métodos get y set
    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getPrimerApellido() {
        return primerApellido;
    }

    public void setPrimerApellido(String primerApellido) {
        this.primerApellido = primerApellido;
    }

    public String getSegundoApellido() {
        return segundoApellido;
    }

    public void setSegundoApellido(String segundoApellido) {
        this.segundoApellido = segundoApellido;
    }

    public String getIdentificacion() {
        return identificacion;
    }

    public void setIdentificacion(String identificacion) {
        this.identificacion = identificacion;
    }

    public String getNombreUsuario() {
        return nombreUsuario;
    }

    public void setNombreUsuario(String nombreUsuario) {
        this.nombreUsuario = nombreUsuario;
    }

    public String getClave() {
        return clave;
    }

    public void setClave(String clave) {
        this.clave = clave;
    }

    public LocalDate getFechaNacimiento() {
        return fechaNacimiento;
    }

    public void setFechaNacimiento(LocalDate fechaNacimiento) {
        this.fechaNacimiento = fechaNacimiento;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public Direccion getDireccion() {
        return direccion;
    }

    public void setDireccion(Direccion direccion) {
        this.direccion = direccion;
    }

//Esto se hace así? Hice un forEach en toString de los dos arrays
    //Metodo para agregar retos -> no encontré ningun ejemplo. Es una relacion de asociación. Se hace igual que con una relación de composición al crear el toString con una variable ArrayList?
    public void agregarRetos(String id, String nombre, String descripcion, String foto, String puntoInicio, String puntoFinal, double kilTotales, double kilRecorridos, String medalla, double tiempoMedalla, double costoReto, int semanasFinReto, TipoActividad tipoActividad) {
        Reto reto = new Reto (id, nombre, descripcion, foto, puntoInicio, puntoFinal, kilTotales, kilRecorridos, medalla, tiempoMedalla, costoReto, semanasFinReto, tipoActividad);
        retos.add(reto);
    }

    //Metodo para agregar metodosDePago
    public void agregarMetodoDePago(String numTarjeta, String proveedorTarjeta, LocalDate fechaVenc, int codigoSeguridad) {
        MetodoPago metodoPago = new MetodoPago (numTarjeta, proveedorTarjeta, fechaVenc, codigoSeguridad);
        metodosPago.add(metodoPago);
    }


   //4. Método toString
    public String toString() {

        String listaRetos= "";
        for(Reto reto: retos){
            listaRetos += reto.toString() + "\n";
        }

       String listaMetodosPago= "";
        for(MetodoPago metodoPago: metodosPago){
            listaMetodosPago += metodoPago.toString() + "\n";
        }

        return "nombre='" + nombre + '\'' +
                ", primerApellido='" + primerApellido + '\'' +
                ", segundoApellido='" + segundoApellido + '\'' +
                ", identificacion='" + identificacion + '\'' +
                ", nombreUsuario='" + nombreUsuario + '\'' +
                ", clave='" + clave + '\'' +
                ", fechaNacimiento=" + fechaNacimiento +
                ", edad=" + edad +
                ", genero='" + genero + '\'' +
                ", direccion=" + direccion +
                ", listaMetodosPago=" + listaMetodosPago +
                ", reto=" + listaRetos +"\n";

    }
}
