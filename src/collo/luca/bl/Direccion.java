package collo.luca.bl;

public class Direccion {

    //1. Atributos privados
    private Pais pais;
    private Provincia provincia;
    private Canton canton;
    private Distrito distrito;

    //2. Método constructor
    public Direccion() {
    }

    public Direccion(Pais pais, Provincia provincia, Canton canton, Distrito distrito) {
        this.pais = pais;
        this.provincia = provincia;
        this.canton = canton;
        this.distrito = distrito;
    }
    //3. Métodos get y set
    public Pais getPais() {
        return pais;
    }

    public void setPais(Pais pais) {
        this.pais = pais;
    }

    public Provincia getProvincia() {
        return provincia;
    }

    public void setProvincia(Provincia provincia) {
        this.provincia = provincia;
    }

    public Canton getCanton() {
        return canton;
    }

    public void setCanton(Canton canton) {
        this.canton = canton;
    }

    public Distrito getDistrito() {
        return distrito;
    }

    public void setDistrito(Distrito distrito) {
        this.distrito = distrito;
    }

    //4. Método toString

    public String toString() {
        return "Direccion{" +
                "pais=" + pais +
                ", provincia=" + provincia +
                ", canton=" + canton +
                ", distrito=" + distrito +
                '}';
    }
}
