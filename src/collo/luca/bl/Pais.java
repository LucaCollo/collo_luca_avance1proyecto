package collo.luca.bl;

public class Pais {
    //1. Atributos privados
    private String id;
    private String nombre;

    //2. Método constructor

    public Pais() {
    }

    public Pais(String id, String nombre) {
        this.id = id;
        this.nombre = nombre;
    }

    //3. Métodos get y set

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    //4. Método toString

    public String toString() {
        return "Pais{" +
                "id='" + id + '\'' +
                ", nombre='" + nombre + '\'' +
                '}';
    }


}
